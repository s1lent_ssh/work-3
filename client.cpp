#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <istream>
#include <iterator>

using namespace std;

const int PORT = 1100;
const int BUFFER_SIZE = 32;
const string MESSAGE = "Предписывает отправить данные как срочные \
(out of band data, OOB). Концепция срочных данных позволяет иметь \
два параллельных канала данных в одном соединении. Иногда это бывает удобно. \
Например, Telnet использует срочные данные для передачи команд типа Ctrl+C. \
В настоящее время использовать их не рекомендуется из-за проблем с совместимостью \
(существует два разных стандарта их использования, описанные в RFC793 и RFC1122). \
Безопаснее просто создать для срочных данных отдельное соединение.";

string file2str(const string& filename) {
	fstream file(filename, ios::binary | ios::in);
	std::string data;
	file.seekg(0, ios::end);   
	data.reserve(file.tellg());
	file.seekg(0, ios::beg);
	data.assign(
		(std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>()
	);
	return std::move(data);
}

int main() {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT), 
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(_socket < 0) {
		cerr << "[Client] Socket create error" << endl;
		return 1;
	}

	int _connect = connect(
		_socket, 
		reinterpret_cast<sockaddr*>(&_sockaddr), 
		sizeof(_sockaddr)
	);
	if(_connect < 0) {
		cerr << "[Client] Socket connect error" << endl;
		close(_socket);
		return 1;
	}

	auto _data = file2str("dragons.jpeg");
	auto _hash = std::to_string(std::hash<string>()(_data));

	send(_socket, _data.c_str(), _data.size(), 0);
	send(_socket, _hash.c_str(), _hash.size(), 0);

	close(_socket);
}