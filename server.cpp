#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

const int PORT = 1100;
const int BUFFER_SIZE = 32;
const int HASH_SIZE = 20;

void createFileFromResponse(const string& response) {
	const auto& _hash = response.substr(
		response.size() - HASH_SIZE + 1, 
		response.size() - 1
	);
	const auto& _data = response.substr(0, response.size() - HASH_SIZE + 1);

	if(_hash == std::to_string(std::hash<string>()(_data))) {
		cout << "[Server] Hashes equals" << endl;
		fstream file("server.jpeg", ios::binary | ios::out);
		file << _data;
		cout << "[Server] File written" << endl; 
	} else {
		cerr << "[Server] Hashes dont equals" << endl;
	}
}

int main() {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT), 
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(_socket < 0) {
		cerr << "[Server] Socket create error" << endl;
		close(_socket);
		return 1;
	}

	int _bind = bind(
		_socket, 
		reinterpret_cast<sockaddr*>(&_sockaddr), 
		sizeof(_sockaddr)
	);
	if(_bind < 0) {
		cerr << "[Server] Socket bind error" << endl;
		close(_socket);
		return 1;
	}

	int _listen = listen(_socket, 10);
	if(_listen < 0) {
		cerr << "[Server] Socket listen error" << endl;
		close(_socket);
		return 1;
	}

	char buffer[BUFFER_SIZE];

	while(true) {
		auto _connection = accept(_socket, NULL, NULL);
		if(_connection < 0) {
			cerr << "[Server] Socket accept error" << endl;
			return 1;
		}
		string response;
		while(true) {
			int readed = recv(_connection, buffer, sizeof(buffer), 0);
			if(readed <= 0) break;
			response += string(buffer, buffer + readed);
		}
		cout << "[Server] Got data: " << response.size() << "B" << endl;
		createFileFromResponse(response);
	}

	return 0;
}